﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour {

	private float initializationTime;

	void Start () {
		initializationTime = Time.timeSinceLevelLoad;
	}

	void OnCollisionEnter2D(Collision2D col)
	{
		String a = col.gameObject.name;
		float timeAlive = Time.timeSinceLevelLoad - initializationTime;

		if(a.StartsWith("Player") && (timeAlive > 0.1f)) {
			Destroy(col.gameObject);
		}
	}
}
