﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirePlayer : MonoBehaviour {

	public float speed = 200f;
	
	public GameObject bullet;
	public GameObject bullet1;

	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown("Fire1")) 
		{
			Vector3 worldMousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			Vector2 direction = (Vector2)((worldMousePos - transform.position));
			direction.Normalize();
			GameObject bullet = (GameObject)Instantiate(
								bullet1,
								transform.position + (Vector3)(direction * 0.5f),
								Quaternion.identity);
			bullet.GetComponent<Rigidbody2D>().velocity = direction * speed; 

		}
	}
	void OnDestroy() {
        print("Script was destroyed");
    }
}
