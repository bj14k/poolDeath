﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine;

public class MovePlayer : MonoBehaviour {
	public float speed = 200.0f;

	void Start () {
	}

	void FixedUpdate () {
		float h = CrossPlatformInputManager.GetAxisRaw ("Horizontal");
		float v = CrossPlatformInputManager.GetAxisRaw ("Vertical");

		Vector2 dir = new Vector2(h, v);
		GetComponent<Rigidbody2D> ().velocity = dir.normalized * speed;

	}

	/*void OnCollisionEnter2D(Collision2D col)
	{
		String a = col.gameObject.name;
		// Debug.Log(col.gameObject.initializationTime);
		if(!a.StartsWith("Bar") || !a.StartsWith("bullet")) {
			Destroy(col.gameObject);
		}
	}*/
}
