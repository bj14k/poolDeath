﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : MonoBehaviour {
	public Color loadToColor = Color.blue;

	public void PlayGame ()
	{
		Initiate.Fade("PoolDeath",loadToColor,0.5f);	
	}

	public void QuitGame ()
	{
		Application.Quit();
	}
}
